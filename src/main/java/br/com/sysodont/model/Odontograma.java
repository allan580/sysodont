package br.com.sysodont.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe modelo de Odontograma.
 *
 * @author allan.richard
 * @since 09/11/2017
 */
@Entity
@Table(name = "odontograma")
public class Odontograma implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "odontograma_id")
    private Long odontogramaId;

    @Column(name = "dente")
    private BigDecimal dente;

    @Column(name = "lado_dente")
    private String ladoDente;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tratamento_id", referencedColumnName = "tratamento_id")
    private Tratamento tratamento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pessoa_id", referencedColumnName = "pessoa_id")
    private Pessoa pessoa;

    @OneToMany(mappedBy = "odontograma", fetch = FetchType.LAZY)
    private List<Pagamento> pagamentos;

    @Column(name = "id_view")
    private String idview;

    public Long getOdontogramaId() {
        return odontogramaId;
    }

    public void setOdontogramaId(Long odontogramaId) {
        this.odontogramaId = odontogramaId;
    }

    public BigDecimal getDente() {
        return dente;
    }

    public void setDente(BigDecimal dente) {
        this.dente = dente;
    }

    public String getLadoDente() {
        return ladoDente;
    }

    public void setLadoDente(String ladoDente) {
        this.ladoDente = ladoDente;
    }

    public Tratamento getTratamento() {
        return tratamento;
    }

    public void setTratamento(Tratamento tratamento) {
        this.tratamento = tratamento;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getIdview() {
        return idview;
    }

    public void setIdview(String idview) {
        this.idview = idview;
    }

    public List<Pagamento> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamento> pagamentos) {
        this.pagamentos = pagamentos;
    }

}
