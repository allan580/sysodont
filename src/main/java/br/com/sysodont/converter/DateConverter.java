package br.com.sysodont.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.ResourceBundle;
import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.converter.Converter;
import br.com.caelum.vraptor.converter.ConversionException;
import br.com.caelum.vraptor.validator.SimpleMessage;
import com.google.common.base.Strings;
import java.text.SimpleDateFormat;
import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.interceptor.Interceptor;


@Alternative
@Priority(Interceptor.Priority.APPLICATION)
@RequestScoped
@Convert(Date.class)
public class DateConverter implements Converter<Date> {

    private final ResourceBundle messagesBundle;

    /**
     * @deprecated CDI eyes only
     */
    public DateConverter() {
        this(null);
    }

    @Inject
    public DateConverter(ResourceBundle messagesBundle) {
        this.messagesBundle = messagesBundle;
    }

    @Override
    public Date convert(String value, Class<? extends Date> type) {
        if (Strings.isNullOrEmpty(value)) {
            return null;
        }

        DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH");
        try {
            return dateTimeFormat.parse(value);
        } catch (ParseException ex) {
            DateFormat dateFormat = new SimpleDateFormat("YY-MM-dd");
            try {
                return dateFormat.parse(value);
            } catch (ParseException ex1) {
                throw new ConversionException(new SimpleMessage("error", "data inválida"));
            }
        }
    }
}
