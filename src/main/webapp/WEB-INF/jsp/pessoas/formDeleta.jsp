<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Pessoas</title>     
    </head>
    <body>
        <div class="container">
            <div>
                <h2>
                    Pessoas
                </h2>
            </div>
            <hr>
            <p>
                Realmente deseja remover a pessoa '${pessoa.nome}'?
            </p>
            <form id="formProduto" action="${linkTo[PessoasController].deleta()}" method="POST">
                <div class="row">
                    <input class="form-control" type="hidden" id="txtMethod" name="_method" value="DELETE"/><br/>
                    <input type="hidden" value="${pessoa.pessoaId}" id="txtPessoaId" name="pessoa.pessoaId"/><br/> 
                </div>
                <div class="btn-group">
                    <input type="submit" value="deletar" class="btn btn-danger" />
                    <a href="${linkTo[PessoasController].lista()}" class="btn btn-default" >Voltar</a>
                </div>
            </form>
        </div>
        <script>
        </script>
    </body>
</html>
