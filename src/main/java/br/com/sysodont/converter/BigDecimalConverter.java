import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.converter.Converter;
import com.google.common.base.Strings;
import java.math.BigDecimal;
import java.util.ResourceBundle;
import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.interceptor.Interceptor;

/**
 * Converter to BigDecimal that overrides the VRaptor default.
 *
 * @author allan.richard/luiz.henrique
 * @since 09/11/2017
 */
@Alternative
@Priority(Interceptor.Priority.APPLICATION)
@RequestScoped
@Convert(BigDecimal.class)
public class BigDecimalConverter implements Converter<BigDecimal> {

    private final ResourceBundle messagesBundle;

    /**
     * @deprecated CDI eyes only
     */
    public BigDecimalConverter() {
        this(null);
    }

    /**
     * Class constructor.
     *
     * @param messagesBundle Refer to the field messagesBundle of entity.
     */
    @Inject
    public BigDecimalConverter(ResourceBundle messagesBundle) {
        this.messagesBundle = messagesBundle;
    }

    @Override
    public BigDecimal convert(String value, Class<? extends BigDecimal> type) {

        if (Strings.isNullOrEmpty(value)) {
            return null;
        }
        value = value.replace(",", ".");
        return new BigDecimal(value);

    }
}