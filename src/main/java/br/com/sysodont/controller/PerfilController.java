package br.com.sysodont.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.com.sysodont.dao.UsuarioDao;
import br.com.sysodont.model.SenhaDto;
import br.com.sysodont.model.Usuario;
import br.com.sysodont.model.UsuarioLogado;
import javax.inject.Inject;

@Controller
@Path("/perfil")
public class PerfilController {

    @Inject
    private UsuarioDao dao;
    @Inject
    private Validator validator;
    @Inject
    private Result result;
    @Inject
    private UsuarioLogado usuarioLogado;

    @Get
    public void perfil() {

    }

    @Post
    public void trocaSenha(SenhaDto senha) {
        if (!senha.getNovaSenha().equals(senha.getConfirmacaoSenha())) {
            validator.add(new SimpleMessage("Troca de Senha", "Confirmação de senha não confere!"));
            validator.onErrorUsePageOf(this).perfil();
        } else if (senha.getSenhaAtual() == null) {
            validator.add(new SimpleMessage("Troca de Senha", "Fornecça a Senha atual!"));
            validator.onErrorUsePageOf(this).perfil();
        } else {
            Usuario usuario = new Usuario();
            usuario.setUsuarioId(usuarioLogado.getUsuario().getUsuarioId());
            usuario.setLogin(usuarioLogado.getUsuario().getLogin());
            usuario.setNivelAcesso(usuarioLogado.getUsuario().getNivelAcesso());
            usuario.setSenha(senha.getSenhaAtual());

            if (dao.validaLoginSenha(usuario) != null) {
                usuario.setSenha(senha.getNovaSenha());
                dao.atualiza(usuario);
                result.redirectTo(this).perfil();
            } else {
                validator.add(new SimpleMessage("Troca de Senha", "Senha atual incorreta!"));
                validator.onErrorUsePageOf(this).perfil();
            }
        }
    }
}
