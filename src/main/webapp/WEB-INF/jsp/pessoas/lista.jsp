<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Pessoas</title>
    </head>
    <body>
        <div class="row">
            <div class="col-md-8">
                <h2>Pessoas</h2>
            </div>
            <div class="col-md-4 text-right">
                <a href="${linkTo[PessoasController].form()}">
                    <button class="btn btn-primary" style="margin-top:20px;"><i class="fa fa-plus" aria-hidden="true"></i>Nova Pessoa</button>
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table id="listaPessoas" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="col-2">ID</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>RG</th>
                        <th>Data de nascimento</th>
                        <th>Telefone</th>
                        <th>Plano</th>
                        <th>Operações</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${pessoaList}" var="pessoa">
                        <tr>
                            <td> ${pessoa.pessoaId} </td>
                            <td> ${pessoa.nome} </td>
                            <td> ${pessoa.cpf} </td>
                            <td> ${pessoa.rg} </td>
                            <td> ${pessoa.dataNascimento} </td>
                            <td> ${pessoa.telefone} </td>
                            <td> ${pessoa.plano.nomePlano} </td>
                            <td> 
                                <div class="btn-group">
                                    <a href="${linkTo[PessoasController].form(pessoa.pessoaId)}" id="btnPessoaAtualiza_${pessoa.pessoaId}" class="btn btn-default">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                    <a href="${linkTo[PessoasController].formDeleta(pessoa.pessoaId)}" id="btnPessoaDeleta_${pessoa.pessoaId}" type="button" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <script src="<c:url value='/resources/application/pessoas/lista.js'/>"></script>
    </body>
</html>
