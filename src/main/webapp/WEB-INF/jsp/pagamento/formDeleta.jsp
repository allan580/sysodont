<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Pagamento</title>     
    </head>
    <body>
        <div class="container">
            <div>
                <h2>
                    Remover Pagamento
                </h2>
            </div>
            <hr>
            <p>
                Deseja remover o pagamento de '${pagamento.odontograma.pessoa.nome}'?
            </p>
            <form id="formPagamento" action="${linkTo[PagamentoController].deleta()}" method="POST">
                <div class="row">
                    <input class="form-control" type="hidden" id="txtMethod" name="_method" value="DELETE"/><br/>
                    <input type="hidden" value="${pagamento.pagamentoId}" id="txtpagamentoId" name="pagamento.pagamentoId"/><br/> 
                </div>
                <div class="btn-group">
                    <input type="submit" value="Deletar" class="btn btn-danger" />
                    <a href="${linkTo[PagamentoController].lista()}" class="btn btn-default" >Voltar</a>
                </div>
            </form>
        </div>
    </body>
</html>
