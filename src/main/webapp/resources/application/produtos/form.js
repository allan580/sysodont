$(document).ready(function () {
    $('#selGrupoProduto').select2({
        ajax: {
            url: $('#pathGrupoPorNomeJson').val(),
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    descricao: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
                return {
                    results: $.map(data.list, function (item) {
                        return {
                            'descricao': item.descricao,
                            'grupoProdutoId': item.grupoProdutoId,
                            text: item.descricao,
                            id: item.grupoProdutoId
                        }
                    })
                };
            },
            cache: true
        },
        allowClear: true,
        theme: "bootstrap",
        placeholder: 'Busca de grupos de produtos',
        minimumInputLength: 2
    });
});