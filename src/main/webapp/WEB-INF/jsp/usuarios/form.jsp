<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Usuários</title> 
    </head>
    <body>
        <c:set var = "formAction" value = "${linkTo[UsuariosController].adiciona()}"/>
        <c:set var = "formMethod" value = "POST"/>
        <c:if test = "${usuario.usuarioId ne null}">
            <c:set var = "formAction" value = "${linkTo[UsuariosController].atualiza(usuario)}"/>
            <c:set var = "formMethod" value = "PUT"/>
        </c:if>
        <div class="container">
            <h2>
                Formulário de Usuário
            </h2>
            <hr>
            <form id="formUsuario" action="${formAction}" method="POST">
                <div class="container-fluid">
                    <input type="hidden" value="${formMethod}" id="txtMethod" name="_method"/>
                    <input type="hidden" value="${usuario.usuarioId}" id="txtUsuarioId" name="usuario.usuarioId"/>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <label for="txtUsuarioDescricao">Login</label>
                                <input type="text" value="${usuario.login}" class="form-control" id="txtUsuarioLogin" name="usuario.login" required="required" minlenght="4" autofocus="true">
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <label for="txtUsuarioDescricao">Senha</label>
                                <input type="password" value="${usuario.senha}" class="form-control" id="txtUsuarioSenha" name="usuario.senha" required="required"  minlenght="6">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="form-group" style="margin-top:20px; margin-right: -600px;">
                            <input type="submit" value="Confirmar" class="btn btn-primary btn-sm" />
                            <a href="${linkTo[UsuariosController].lista()}" class="btn btn-default btn-sm"> 
                                Voltar
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>