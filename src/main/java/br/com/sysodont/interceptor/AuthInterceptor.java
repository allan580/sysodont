package br.com.sysodont.interceptor;

import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;
import br.com.sysodont.anotacoes.Publico;
import br.com.sysodont.controller.LoginController;
import br.com.sysodont.model.UsuarioLogado;
import javax.inject.Inject;

/**
 * Interceptor para autenticação de usuário.
 *
 * @author allan.richard
 * @since 02/11/2017
 */
@Intercepts
public class AuthInterceptor {

    @Inject
    private UsuarioLogado usuarioLogado;
    @Inject
    private Result result;
    @Inject
    private ControllerMethod controllerMethod;

    @Accepts
    public Boolean accepts() {
        return !controllerMethod.containsAnnotation(Publico.class);
    }

    @AroundCall
    public void intercepta(SimpleInterceptorStack stack) {
        if (usuarioLogado.getUsuario() == null) {
            result.redirectTo(LoginController.class).form();
            return;
        }
        stack.next();
    }

}
