<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Produto</title> 
    </head>
    <body>
        <c:set var = "formAction" value = "${linkTo[ProdutosController].adiciona()}"/>
        <c:set var = "formMethod" value = "POST"/>
        <c:if test = "${produto.produtoId ne null}">
            <c:set var = "formAction" value = "${linkTo[ProdutosController].atualiza(produto)}"/>
            <c:set var = "formMethod" value = "PUT"/>
        </c:if>
        <div class="container">
            <h2>
                Formulário de Produto
            </h2>
            <hr>
            <form id="formProduto" action="${formAction}" method="POST">
                <div class="container-fluid">
                    <input type="hidden" value="${formMethod}" id="txtMethod" name="_method"/>
                    <input type="hidden" value="${produto.produtoId}" id="txtProdutoId" name="produto.produtoId"/>
                    <div class="row">
                        <div class="form-group col-lg-10 col-md-10 col-sm-10">
                            <label for="txtProdutoDescricao">Descrição</label>
                            <input type="text" value="${produto.descricao}" class="form-control" id="txtProdutoDescricao" name="produto.descricao" required="required" autofocus="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-2 col-md-2 col-sm-2">
                            <label for="txtQuantidadeProduto">Quantidade Produto</label>
                            <input type="number" value="${produto.quantidadeProduto}" class="form-control" id="txtQuantidadeProduto" name="produto.quantidadeProduto" required="required" autofocus="true">
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2">
                            <label for="txtProdutoPeso">Preço</label>
                            <input type="number" value="${produto.preco}" class="form-control" id="txtProdutoPreco" name="produto.preco" min="0">
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="form-group" style="margin-top:20px; margin-right: -600px;">
                            <input type="submit" value="Confirmar" class="btn btn-primary btn-sm" />
                            <a href="${linkTo[ProdutosController].lista()}" class="btn btn-default btn-sm"> 
                                Voltar
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <input type="hidden" id="pathGrupoPorNomeJson" value="${linkTo[GruposProdutosController].buscaPorDescricao()}" />
        <script src="<c:url value='/resources/application/produtos/form.js'/>"></script>
    </body>
</html>
