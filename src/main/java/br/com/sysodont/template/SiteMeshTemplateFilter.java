package br.com.sysodont.template;

import java.io.IOException;
import org.sitemesh.DecoratorSelector;
import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.sitemesh.content.Content;
import org.sitemesh.webapp.WebAppContext;

/**
 * Filter para aplicação do sitemesh.
 *
 * @author andre.baroni
 * @since 27/09/2017
 */
public class SiteMeshTemplateFilter extends ConfigurableSiteMeshFilter {

    /**
     * Aplica configuração.
     *
     * @param builder Referente ao SiteMeshFilterBuilder.
     */
    @Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
        builder.setCustomDecoratorSelector(new CustomDecoratorSelector());
    }

    /**
     * Instancia um novo selector.
     */
    private class CustomDecoratorSelector implements DecoratorSelector<WebAppContext> {

        @Override
        public String[] selectDecoratorPaths(Content content, WebAppContext context) throws IOException {
            String decorator = "default";
            if (content.getExtractedProperties() != null
                    && content.getExtractedProperties().getChild("meta") != null
                    && content.getExtractedProperties().getChild("meta").getChild("decorator") != null
                    && content.getExtractedProperties().getChild("meta").getChild("decorator").getValue() != null
                    && !content.getExtractedProperties().getChild("meta").getChild("decorator").getValue().isEmpty()) {
                decorator = content.getExtractedProperties().getChild("meta").getChild("decorator").getValue();
            }

            if (decorator.equals("null")) {
                return new String[0];
            }

            if (context.getRequest().getParameter("modal") != null && Boolean.valueOf(context.getRequest().getParameter("modal"))) {
                decorator = "blank";
            }

            return new String[]{"/WEB-INF/templates/" + decorator + ".jsp"};
        }
    }
}
