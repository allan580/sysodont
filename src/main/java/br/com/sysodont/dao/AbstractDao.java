package br.com.sysodont.dao;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 * Implementação de DAO abstrato.
 *
 * @author andre.baroni
 * @param <Entity>
 * @since 28/09/2017
 */
public abstract class AbstractDao<Entity> {

    @Inject
    private EntityManager entityManager;

    protected EntityManager getEntityManager() {
        return this.entityManager;
    }

    protected abstract Class<Entity> getEntityClass();

    public List<Entity> listaTodos() {
        CriteriaQuery<Entity> cq = getEntityManager().getCriteriaBuilder()
                .createQuery(getEntityClass());
        cq.select(cq.from(getEntityClass()));

        return getEntityManager().createQuery(cq).getResultList();
    }

    public void adiciona(Entity entity) {
        getEntityManager().getTransaction().begin();
        try {
            getEntityManager().persist(entity);
        } finally {
            getEntityManager().flush();
            getEntityManager().getTransaction().commit();
        }
    }

    public void atualiza(Entity entity) {
        getEntityManager().getTransaction().begin();
        try {
            getEntityManager().merge(entity);
        } finally {
            getEntityManager().flush();
            getEntityManager().getTransaction().commit();
        }
    }

    public void deleta(Entity entity) {
        getEntityManager().getTransaction().begin();
        try {
            getEntityManager().remove(entity);
        } finally {
            getEntityManager().flush();
            getEntityManager().getTransaction().commit();
        }
    }

    public Entity buscaPorId(Long id) {
        return getEntityManager().find(getEntityClass(), id);
    }

    public Boolean existe(Entity entity) {
        return (buscaPorId(getEntityIdentifier(entity)) != null);
    }

    public Long getEntityIdentifier(Entity entity) {
        return (Long) getEntityManager().getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);
    }
}
