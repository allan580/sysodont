<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Tratamento</title> 
    </head>
    <body>
        <c:set var = "formAction" value = "${linkTo[TratamentosController].adiciona()}"/>
        <c:set var = "formMethod" value = "POST"/>
        <c:if test = "${tratamento.tratamentoId ne null}">
            <c:set var = "formAction" value = "${linkTo[TratamentosController].atualiza(tratamento)}"/>
            <c:set var = "formMethod" value = "PUT"/>
        </c:if>
        <div class="container">
            <h2>
                Formulário de Tratamento
            </h2>
            <hr>
            <form id="formTratamento" action="${formAction}" method="POST">
                <div class="container-fluid">
                    <input type="hidden" value="${formMethod}" id="txtMethod" name="_method"/>
                    <input type="hidden" value="${tratamento.tratamentoId}" id="txtTratamentoId" name="tratamento.tratamentoId"/>
                    <div class="row">
                        <div class="form-group col-lg-8 col-md-8 col-sm-8">
                            <label for="txtTratamentoDescricao">Descrição</label>
                            <input type="text" value="${tratamento.descricao}" class="form-control" id="txtTratamentoDescricao" name="tratamento.descricao" required="required" autofocus="true">
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2">
                            <label for="txtTratamentoPeso">Preço</label>
                            <input type="number" value="${tratamento.preco}" class="form-control" id="txtTratamentoPreco" name="tratamento.preco" min="0">
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="form-group" style="margin-top:20px; margin-right: -600px;">
                            <input type="submit" value="Confirmar" class="btn btn-primary btn-sm" />
                            <a href="${linkTo[TratamentosController].lista()}" class="btn btn-default btn-sm"> 
                                Voltar
                            </a>
                        </div>
                    </div>
                </div>

                </div>
            </form>
        </div>
        <input type="hidden" id="pathGrupoPorNomeJson" value="${linkTo[GruposTratamentosController].buscaPorDescricao()}" />
        <script src="<c:url value='/resources/application/tratamentos/form.js'/>"></script>
    </body>
</html>
