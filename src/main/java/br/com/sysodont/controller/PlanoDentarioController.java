package br.com.sysodont.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import static br.com.caelum.vraptor.view.Results.json;
import br.com.sysodont.dao.PlanoDentarioDao;
import br.com.sysodont.model.PlanoDentario;
import br.com.sysodont.model.Produto;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Classe Controller de {@link Produto}.
 *
 * @author allan.richard/luiz derenzi
 * @since 18/10/2017
 */
@Controller
@Path("/planodentario")
public class PlanoDentarioController {

    @Inject
    private PlanoDentarioDao dao;

    @Inject
    private Result result;

    @Get("/")
    public List<PlanoDentario> lista() {
        return dao.listaTodos();
    }

    @Path("/form")
    public void form() {

    }

    @Get("/form/{id}")
    public void form(Long id) {
        PlanoDentario planoDentario = dao.buscaPorId(id);
        if (planoDentario != null) {
            result.include("planoDentario", planoDentario);
        } else {
            result.redirectTo(PlanoDentarioController.class).registroNaoEncontrado();
        }
    }

    @Post("/")
    public void adiciona(@Valid PlanoDentario planoDentario) {
        dao.adiciona(planoDentario);
        result.redirectTo(PlanoDentarioController.class).lista();
    }

    @Put("/{plano.planoId}")
    public void atualiza(@Valid PlanoDentario planoDentario) {
        dao.atualiza(planoDentario);
        result.redirectTo(PlanoDentarioController.class).lista();
    }

    @Get("/form-deleta/{id}")
    public void formDeleta(Long id) {
        PlanoDentario planoDentario = dao.buscaPorId(id);
        result.include("planoDentario", planoDentario);
    }

    @Delete("/")
    public void deleta(PlanoDentario planoDentario) {
        PlanoDentario localPlanoDentario = dao.buscaPorId(planoDentario.getPlanoId());
        dao.deleta(localPlanoDentario);
        result.redirectTo(PlanoDentarioController.class).lista();
    }

    @Get("/nao-encontrado")
    public void registroNaoEncontrado() {

    }

    @Get("/por-descricao-json")
    public void buscaPorNome(String nome) {
        List<PlanoDentario> planos = dao.buscaPorNome(nome);
        if (planos != null) {
            result.use(json()).from(planos)
                    .serialize();
        } else {
            result.use(json()).serializeNulls();
        }
    }
}