package br.com.sysodont.controller;


import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.com.sysodont.dao.UsuarioDao;
import br.com.sysodont.model.Usuario;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Classe Controller de {@link Usuario}.
 *
 * @author allan.richard
 * @since 04/10/2017
 */
@Controller
@Path("/usuarios")
public class UsuariosController {

    @Inject
    private UsuarioDao dao;

    @Inject
    private Result result;

    @Inject
    private Validator validator;

    @Get("/")
    public List<Usuario> lista() {
        return dao.listaTodos();
    }

    @Path("/form")
    public void form() {

    }

    @Get("/form/{id}")
    public void form(Long id) {
        Usuario usuario = dao.buscaPorId(id);
        if (usuario != null) {
            result.include("usuario", usuario);
        } else {
            result.redirectTo(UsuariosController.class).registroNaoEncontrado();
        }
    }

    @Put("/{usuario.usuarioId}")
    public void atualiza(@Valid Usuario usuario) {
        dao.atualiza(usuario);
        result.redirectTo(UsuariosController.class).lista();
    }

    @Post("/")
    public void adiciona(@Valid Usuario usuario) {
        if (dao.buscaPorLogin(usuario.getLogin()) != null) {
            validator.add(new SimpleMessage("Usuário", "Usuário já cadastrado!"));
            validator.onErrorRedirectTo(this).lista();
        }
        dao.adiciona(usuario);

        result.redirectTo(UsuariosController.class).lista();
    }

    @Get("/form-deleta/{id}")
    public void formDeleta(Long id) {
        Usuario usuario = dao.buscaPorId(id);
        if (usuario != null) {
            result.include("usuario", usuario);
        } else {
            registroNaoEncontrado();
        }
    }

    @Delete("/")
    public void deleta(Usuario usuario) {
        Usuario localUsuario = dao.buscaPorId(usuario.getUsuarioId());
        dao.deleta(localUsuario);
        result.redirectTo(UsuariosController.class).lista();
    }

    @Get("/nao-encontrado")
    public void registroNaoEncontrado() {

    }
}
