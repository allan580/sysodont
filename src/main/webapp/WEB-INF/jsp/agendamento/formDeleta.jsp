<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Agendamento</title>     
    </head>
    <body>
        <div class="container">
            <div>
                <h2>
                    Remover Agendamento
                </h2>
            </div>
            <hr>
            <p>
                Deseja remover o agendamento de'${agendamento.pessoas.nome}'?
            </p>
            <form id="formAgendamento" action="${linkTo[AgendamentoController].deleta()}" method="POST">
                <div class="row">
                    <input class="form-control" type="hidden" id="txtMethod" name="_method" value="DELETE"/><br/>
                    <input type="hidden" value="${agendamento.agendamentoId}" id="txtAgendamentoId" name="agendamento.agendamentoId"/><br/> 
                </div>
                <div class="btn-group">
                    <input type="submit" value="Deletar" class="btn btn-danger" />
                    <a href="${linkTo[AgendamentoController].lista()}" class="btn btn-default" >Voltar</a>
                </div>
            </form>
        </div>
    </body>
</html>
