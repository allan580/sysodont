package br.com.sysodont.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.com.sysodont.anotacoes.Publico;
import br.com.sysodont.dao.UsuarioDao;
import br.com.sysodont.model.Usuario;
import br.com.sysodont.model.UsuarioLogado;
import javax.inject.Inject;

@Controller
@Path("/login")
public class LoginController {

    @Inject
    private UsuarioDao dao;
    @Inject
    private Validator validator;
    @Inject
    private Result result;
    @Inject
    private UsuarioLogado usuarioLogado;

    @Get("/form")
    @Publico
    public void form() {

    }

    @Post
    @Publico
    public void autentica(Usuario usuario) {
        Usuario usuarioLocal = dao.validaLoginSenha(usuario);
        if (usuarioLocal == null) {
            validator.add(new SimpleMessage("Autenticação", "usuário ou senha inválido!"));
            validator.onErrorRedirectTo(this).form();
        }
        usuarioLogado.setUsuario(usuarioLocal);
        result.redirectTo(IndexController.class).index();
    }

    @Get
    public void desloga() {
        if (usuarioLogado.estaLogado()) {
            usuarioLogado.setUsuario(null);
            result.redirectTo(this).form();
        }
    }
}