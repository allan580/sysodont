<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Login
        </title>
    </head>
    <body>
        <div class="container">
            <c:if test="${errors ne null}">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <c:forEach items="${errors}" var="erro">
                        ${erro.category} - ${erro.message} <br/>
                    </c:forEach>
                </div>
            </c:if>
            <h2>
                Perfil de Usuário
            </h2>
            <hr>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <label for="txtLogin">Login</label>
                        <input type="text" value="${usuarioLogado.usuario.login}" class="form-control" id="txtLogin" name="usuario.login" disabled="true">
                    </div>
                </div>
                <div class="row">
                    <button class="btn btn-primary" onclick="showModalTrocaSenha()" style="margin-top: 20px; margin-left: 16px;">
                        Trocar Senha
                    </button>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalTrocaSenha" role="dialog" aria-labelledby="logoutModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="logoutModalLabel">Troca de senha</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="${linkTo[PerfilController].trocaSenha()}" method="POST">
                        <input type="hidden" value="POST" id="txtMethod" name="_method"/>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <label for="txtSenhaAtual">Senha atual</label>
                                        <input type="password" class="form-control" id="txtSenhaAtual" name="senha.senhaAtual" required="true" autofocus="true">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <label for="txtNovaSenha">Nova Senha</label>
                                        <input type="password" class="form-control" id="txtNovaSenha" name="senha.novaSenha" minlength="6" required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <label for="txtConfirmacaoSenha">Confirmação de Senha</label>
                                        <input type="password" class="form-control" id="txtConfirmacaoSenha" name="senha.confirmacaoSenha" minlength="6" required="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" value="Trocar Senha">
                            <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <input type="hidden" value="${linkTo[PerfilController].trocaSenha()}" id="pathTrocaSenha">
        <script src="<c:url value="/resources/application/perfil/perfil.js"/>"></script>
    </body>
</html>