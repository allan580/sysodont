<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Tratamento</title>
    </head>
    <body>
        <div class="row">
            <div class="col-md-8">
                <h2>Tratamentos</h2>
            </div>
            <div class="col-md-4 text-right">
                <a href="${linkTo[TratamentosController].form()}">
                    <button class="btn btn-primary" style="margin-top:20px;"><i class="fa fa-plus" aria-hidden="true"></i>Novo Tratamento</button>
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table id="listaTratamento" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="col-2">ID</th>
                        <th>Descrição</th>
                        <th>Preço</th>
                        <th>Operações</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${tratamentoList}" var="tratamento">
                        <tr>
                            <td> ${tratamento.tratamentoId} </td>
                            <td> ${tratamento.descricao} </td>
                            <td> ${tratamento.preco} </td>
                            <td> 
                                <div class="btn-group">
                                    <a href="${linkTo[TratamentosController].form(tratamento.tratamentoId)}" id="btnTratamentoAtualiza_${tratamento.tratamentoId}" class="btn btn-default">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                    <a href="${linkTo[TratamentosController].formDeleta(tratamento.tratamentoId)}" id="btnTratamentoDeleta_${tratamento.tratamentoId}" type="button" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <script src="<c:url value='/resources/application/tratamentos/lista.js'/>"></script>
    </body>
</html>
