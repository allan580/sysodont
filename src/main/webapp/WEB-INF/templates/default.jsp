<%@page import="javax.servlet.jsp.jstl.core.Config"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            SysOdont
            <sitemesh:write property='title'/>
        </title>
        <!-- jQuery 3 -->
        <script type="text/javascript" src="/sysodont/dist/js/jquery-3.2.1.min.js" async></script>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="/sysodont/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/sysodont/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/sysodont/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/sysodont/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/sysodont/dist/css/skins/_all-skins.min.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="/sysodont/bower_components/morris.js/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="/sysodont/bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="/sysodont/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="/sysodont/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="/sysodont/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- select2 css import -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
        <link href="<c:url value="/resources/css/select2-bootstrap.min.css"/>" type="text/css" rel="stylesheet" />
        
        <!-- import img e css odontograma -->
        <link rel="stylesheet" href="/sysodont/css/estilosOdontograma.css" />
        <link rel="stylesheet" href="/sysodont/imagens" />
        <!-- datetimepicker -->
        <link rel="stylesheet" href="/sysodont/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <sitemesh:write property='head'/>
    <title>
        SysOdont - Sistema Odontol�gico
        <sitemesh:write property='title'/>
    </title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <header class="main-header">
        <!-- Logo -->
        <a href="${linkTo[IndexController].index()}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>S</b>yS</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>SysOdont</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-fw fa fa-user"></i>${usuarioLogado.usuario.login}
                        </a>
                        <ul class="dropdown-menu">
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <h6 class="dropdown-header">Op��es:</h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="${linkTo[PerfilController].perfil()}">
                                    <i class="fa fa-fw fa-user"></i>Perfil</a>

                                <a class="dropdown-item" href="#!" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fa fa-fw fa-sign-out"></i>Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- search form -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Menu Principal</li>
                <li class="active treeview menu-open">
                    <a href="#">
                        <i class="fa fa-plus"></i> <span>Cadastros</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="${linkTo[AgendamentoController].lista()}">Agendamento</a></li>
                        <li class="active"><a href="${linkTo[ProdutosController].lista()}">Produtos</a></li>
                        <li class="active"><a href="${linkTo[TratamentosController].lista()}">Tratamentos</a></li>
                        <li class="active"><a href="${linkTo[PlanoDentarioController].lista()}">Plano Dent�rio</a></li>
                        <li class="active"><a href="${linkTo[PessoasController].lista()}">Pessoas</a></li>
                    </ul>
                </li>
                <li class="active treeview menu-open">
                    <a href="#">
                        <i class="fa fa-cogs"></i> <span>Manuten��es</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="${linkTo[OdontogramaController].lista()}">Odontograma</a></li>
                    </ul>
                </li>
                <li class="active treeview menu-open">
                    <a href="#">
                        <i class="fa fa-money"></i> <span>Finan�as</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="${linkTo[PagamentoController].lista()}">Pagamentos</a></li>
                    </ul>
                </li>
                <li class="active treeview menu-open">
                    <a href="#">
                        <i class="fa fa-shield"></i> <span>Seguran�a</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="${linkTo[UsuariosController].lista()}">Usu�rios</a></li>
                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <div class="container-fluid">
                <sitemesh:write property='body'/>
            </div>
        </section>
        <!-- /.content-wrapper -->
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logoutModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="logoutModalLabel">Sair</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">?</span>
                        </button>
                    </div>
                    <div class="modal-body">Realmente deseja sair do sistema?</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="${linkTo[LoginController].desloga()}">Sair</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2017 <a href="#">Allan Richard / Luiz Henrique</a>.</strong> All rights
        reserved.
    </footer>

    <!-- Control Sidebar -->
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/sysodont/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <!-- select2 js import -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <!-- Morris.js charts -->
    <script src="/sysodont/bower_components/raphael/raphael.min.js"></script>
    <!-- Sparkline -->
    <script src="/sysodont/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="/sysodont/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/sysodont/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="/sysodont/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="/sysodont/bower_components/moment/min/moment.min.js"></script>
    <script src="/sysodont/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="/sysodont/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="/sysodont/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="/sysodont/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="/sysodont/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="/sysodont/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="/sysodont/dist/js/pages/dashboard.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <!-- AdminLTE for demo purposes -->
    <!-- DateTimePicker -->
    <script type="text/javascript" src="/sysodont/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="/sysodont/bower_components/moment/min/moment.min.js"></script>
    <script src="/sysodont/dist/js/demo.js"></script>
    <script src="/sysodont/bower_components/morris.js/morris.min.js"></script>
</body>
</html>