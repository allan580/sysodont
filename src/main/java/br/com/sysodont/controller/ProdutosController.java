package br.com.sysodont.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.sysodont.dao.ProdutoDao;
import br.com.sysodont.model.Produto;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Classe Controller de {@link Produto}.
 *
 * @author allan.richard/luiz derenzi
 * @since 05/10/2017
 */
@Controller
@Path("/produtos")
public class ProdutosController {

    @Inject
    private ProdutoDao dao;

    @Inject
    private Result result;

    @Get("/")
    public List<Produto> lista() {
        return dao.listaTodos();
    }

    @Path("/form")
    public void form() {

    }

    @Get("/form/{id}")
    public void form(Long id) {
        Produto produto = dao.buscaPorId(id);
        if (produto != null) {
            result.include("produto", produto);
        } else {
            result.redirectTo(ProdutosController.class).registroNaoEncontrado();
        }
    }

     @Post("/")
    public void adiciona(@Valid Produto produto) {
        dao.adiciona(produto);
        result.redirectTo(ProdutosController.class).lista();
    }

    @Put("/{produto.produtoId}")
    public void atualiza(@Valid Produto produto) {
        dao.atualiza(produto);
        result.redirectTo(ProdutosController.class).lista();
    }
    
    @Get("/form-deleta/{id}")
    public void formDeleta(Long id) {
        Produto produto = dao.buscaPorId(id);
        result.include("produto", produto);
    }

    @Delete("/")
    public void deleta(Produto produto) {
        Produto localProduto = dao.buscaPorId(produto.getProdutoId());
        dao.deleta(localProduto);
        result.redirectTo(ProdutosController.class).lista();
    }

    @Get("/nao-encontrado")
    public void registroNaoEncontrado() {

    }
}
