<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Usuários</title>
    </head>
    <body>
        <c:if test="${errors ne null}">
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <c:forEach items="${errors}" var="erro">
                    ${erro.category} - ${erro.message} <br/>
                </c:forEach>
            </div>
        </c:if>
        <div class="row">
            <div class="col-md-8">
                <h2>Usuário</h2>
            </div>
            <div class="col-md-4 text-right">
                <a href="${linkTo[UsuariosController].form()}">
                    <button class="btn btn-primary" style="margin-top:20px;"><i class="fa fa-plus" aria-hidden="true"></i>Novo Usuário</button>
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table id="listaUsuario" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th class="col-10">Login</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${usuarioList}" var="usuario">
                        <tr>
                            <td> ${usuario.usuarioId} </td>
                            <td> ${usuario.login} </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <script src="<c:url value='/resources/application/usuarios/lista.js'/>"></script>
    </body>
</html>
