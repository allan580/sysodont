package br.com.sysodont.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import static br.com.caelum.vraptor.view.Results.json;
import br.com.sysodont.dao.PessoaDao;
import br.com.sysodont.model.Pessoa;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Classe Controller de {@link Pessoa}.
 *
 * @author allan.richard/luiz derenzi
 * @since 02/10/2017
 */
@Controller
@Path("/pessoas")
public class PessoasController {

    @Inject
    private PessoaDao dao;

    @Inject
    private Result result;

    @Inject
    private Validator validator;

    @Get("/")
    public List<Pessoa> lista() {
        return dao.listaTodos();
    }

    @Path("/form")
    public void form() {

    }

    @Get("/form/{id}")
    public void form(Long id) {
        Pessoa pessoa = dao.buscaPorId(id);
        if (pessoa != null) {
            result.include("pessoa", pessoa);
        } else {
            result.redirectTo(PessoasController.class).registroNaoEncontrado();
        }
    }

    @Put("/{pessoa.pessoaId}")
    public void atualiza(@Valid Pessoa pessoa) {
        if (pessoa.getPlano().getPlanoId() == null)
        {
            pessoa.setPlano(null);
        }
        dao.atualiza(pessoa);
        result.redirectTo(PessoasController.class).lista();
    }

    @Post("/")
    public void adiciona(@Valid Pessoa pessoa) {
        if (pessoa.getPlano().getPlanoId() == null)
        {
            pessoa.setPlano(null);
        }
        dao.adiciona(pessoa);
        result.redirectTo(PessoasController.class).lista();
    }

    @Get("/form-deleta/{id}")
    public void formDeleta(Long id) {
        Pessoa pessoa = dao.buscaPorId(id);
        result.include("pessoa", pessoa);
    }

    @Delete("/")
    public void deleta(Pessoa pessoa) {
        Pessoa localPessoa = dao.buscaPorId(pessoa.getPessoaId());
        dao.deleta(localPessoa);
        result.redirectTo(PessoasController.class).lista();
    }

    @Get("/nao-encontrado")
    public void registroNaoEncontrado() {

    }

    @Get("/pessoa-por-nome")
    public void buscaPorNome(String nome) {
        List<Pessoa> pessoas = dao.buscaPorNome(nome);
        if (pessoas != null) {
            result.use(json()).from(pessoas)
                    .serialize();
        } else {
            result.use(json()).serializeNulls();
        }
    }
}
