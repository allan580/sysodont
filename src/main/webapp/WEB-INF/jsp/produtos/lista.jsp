<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Produto</title>
    </head>
    <body>

        <div class="row">
            <div class="col-md-8">
                <h2>Produtos</h2>
            </div>
            <div class="col-md-4 text-right">
                <a href="${linkTo[ProdutosController].form()}">
                    <button class="btn btn-primary" style="margin-top:20px;"><i class="fa fa-plus" aria-hidden="true"></i>Novo Produto</button>
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table id="listaProduto" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="col-2">ID</th>
                        <th>Descrição</th>
                        <th>Quantidade Produto</th>
                        <th>Preço</th>                
                        <th>Operações</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${produtoList}" var="produto">
                        <tr>
                            <td> ${produto.produtoId} </td>
                            <td> ${produto.descricao} </td>
                            <td> ${produto.quantidadeProduto} </td>
                            <td> ${produto.preco} </td>
                            <td> 
                                <div class="btn-group">
                                    <a href="${linkTo[ProdutosController].form(produto.produtoId)}" id="btnProdutoAtualiza_${produto.produtoId}" class="btn btn-default">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                    <a href="${linkTo[ProdutosController].formDeleta(produto.produtoId)}" id="btnProdutoDeleta_${produto.produtoId}" type="button" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <script src="<c:url value='/resources/application/produtos/lista.js'/>"></script>
    </body>
</html>
