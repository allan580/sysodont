package br.com.sysodont.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import static br.com.caelum.vraptor.view.Results.json;
import br.com.sysodont.dao.OdontogramaDao;
import br.com.sysodont.dao.PagamentoDao;
import br.com.sysodont.dao.PessoaDao;
import br.com.sysodont.model.Odontograma;
import br.com.sysodont.model.Pagamento;
import br.com.sysodont.model.Pessoa;
import br.com.sysodont.model.Tratamento;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Classe Controller de {@link Pagamento}.
 *
 * @author allan.richard/luiz derenzi
 * @since 02/10/2017
 */
@Controller
@Path("/pagamentos")
public class PagamentoController {

    @Inject
    private PessoaDao pessoaDao;

    @Inject
    private OdontogramaDao odontogramaDao;

    @Inject
    private PagamentoDao dao;

    @Inject
    private Result result;

    @Inject
    private Validator validator;

    @Get("/")
    public List<Pagamento> lista() {
        return dao.listaTodos();
    }

    @Get("/json/filtro/pessoa")
    public void listaJson(Long pessoaId) {
        Pessoa pessoa = pessoaDao.buscaPorId(pessoaId);
        List<Pagamento> pags = dao.buscaPorPessoa(pessoa);
        if (pags != null) {
            result.use(json()).from(pags).serialize();
        } else {
            result.use(json()).serializeNulls();
        }
    }

    @Path("/form")
    public void form() {

    }

    @Get("/form/{id}")
    public void form(Long id) {
        Pagamento pagamento = dao.buscaPorId(id);
        if (pagamento != null) {
            result.include("pagamento", pagamento);
        } else {
            result.redirectTo(PagamentoController.class).registroNaoEncontrado();
        }
    }

    @Put("/{pagamento.pagamentoId}")
    public void atualiza(@Valid Pagamento pagamento) {
        dao.atualiza(pagamento);
        result.redirectTo(PagamentoController.class).lista();
    }

    @Post("/")
    public void adiciona(@Valid Pagamento pagamento, Long pessoaId, int diaVencimento) {
        for (int i = 0; i < pagamento.getQuantidadeParcela(); i++) {
            List<Odontograma> odontogramas = odontogramaDao.buscaPorPessoa(new Pessoa(pessoaId));
            Pagamento pagamentoLocal = new Pagamento();
            pagamentoLocal.setOdontograma(odontogramas.get(0));
            pagamentoLocal.setValorParcela(pagamento.getValorTotal() / pagamento.getQuantidadeParcela());
            pagamentoLocal.setValorTotal(pagamento.getValorTotal());
            pagamentoLocal.setQuantidadeParcela(pagamento.getQuantidadeParcela());
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, diaVencimento);
            calendar.set(Calendar.MONTH, new Date().getMonth() + (i + 1));
            calendar.set(Calendar.YEAR, new Date().getYear());
            pagamentoLocal.setDataVencimento(calendar.getTime());
            dao.adiciona(pagamentoLocal);
        }
        result.redirectTo(PagamentoController.class).lista();
    }

    @Get("/form-deleta/{id}")
    public void formDeleta(Long id) {
        Pagamento pagamento = dao.buscaPorId(id);
        result.include("pagamento", pagamento);
    }

    @Delete("/")
    public void deleta(Pagamento pagamento) {
        Pagamento localPagamento = dao.buscaPorId(pagamento.getPagamentoId());
        dao.deleta(localPagamento);
        result.redirectTo(PagamentoController.class).lista();
    }

    @Get("/nao-encontrado")
    public void registroNaoEncontrado() {

    }

    @Get("/json/filtro/pessoa/{pessoaId}")
    public void valorTotalPagarJson(Long pessoaId) {
        Pessoa pessoa = pessoaDao.buscaPorId(pessoaId);
        List<Tratamento> listaTratamento = dao.buscaTratamentoPessoa(pessoa);

        BigDecimal valorTotal = new BigDecimal("0");
        if (listaTratamento.size() > 0) {
            for (Tratamento tratamento : listaTratamento) {
                valorTotal = valorTotal.add(new BigDecimal(tratamento.getPreco().toString()));
            }
        }
        result.use(json()).from(valorTotal).serialize();
    }
}
