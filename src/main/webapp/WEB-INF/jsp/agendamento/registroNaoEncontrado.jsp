<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Agendamento</title> 
    </head>
    <body>
        <div class="container">
            <h1>
                404 - Não Encontrado
            </h1>
            <h4>
                Ops... não conseguimos encontrar o produto!
            </h4>
            <a href="${linkTo[AgendamentosController].lista()}" class="btn btn-default" >Voltar</a>
        </div>
    </body>
</html>
