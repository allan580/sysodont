$(document).ready(function () {
    $('#selPlano').select2({
        ajax: {
            url: $('#pathPlanoPorDescricaoJson').val(),
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    nome: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
                return {
                    results: $.map(data.list, function (item) {
                        return {
                            'nomePlano': item.nomePlano,
                            'planoId': item.planoId,
                            text: item.nomePlano,
                            id: item.planoId
                        }
                    })
                };
            },
            cache: true
        },
        allowClear: true,
        theme: "bootstrap",
        placeholder: 'Busca de planos',
        minimumInputLength: 2
    });
});