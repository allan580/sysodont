<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Produto</title>     
    </head>
    <body>
        <div class="container">
            <div>
                <h2>
                    Remover Produto
                </h2>
            </div>
            <hr>
            <p>
                Deseja remover o produto '${produto.descricao}'?
            </p>
            <form id="formProduto" action="${linkTo[ProdutosController].deleta()}" method="POST">
                <div class="row">
                    <input class="form-control" type="hidden" id="txtMethod" name="_method" value="DELETE"/><br/>
                    <input type="hidden" value="${produto.produtoId}" id="txtProdutoId" name="produto.produtoId"/><br/> 
                </div>
                <div class="btn-group">
                    <input type="submit" value="Deletar" class="btn btn-danger" />
                    <a href="${linkTo[ProdutosController].lista()}" class="btn btn-default" >Voltar</a>
                </div>
            </form>
        </div>
    </body>
</html>
