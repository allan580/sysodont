package br.com.sysodont.dao;

import br.com.sysodont.model.PlanoDentario;
import br.com.sysodont.model.Produto;
import br.com.sysodont.model.QPlanoDentario;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;

/**
 * Implementaçao da classe DAO de {@link Produto}.
 *
 * @author allan.richard/luiz derenzi
 * @since 18/10/2017
 */
public class PlanoDentarioDao extends AbstractDao<PlanoDentario> {

    @Override
    protected Class<PlanoDentario> getEntityClass() {
        return PlanoDentario.class;
    }

    public List<PlanoDentario> buscaPorNome(String nome) {
        QPlanoDentario qPlanoDentario = QPlanoDentario.planoDentario;

        JPAQueryFactory factory = new JPAQueryFactory(getEntityManager());
        JPAQuery query = factory.query();

        query.select(qPlanoDentario)
                .from(qPlanoDentario)
                .where(qPlanoDentario.nomePlano.contains(nome));

        List<PlanoDentario> planos = query.fetch();

        if (planos.size() > 0) {
            return planos;
        } else {
            return null;
        }
    }
}
