package br.com.sysodont.model;

import java.util.Objects;

/**
 * DTO para troca de senha.
 *
 * @author allan.richard
 * @since 02/11/2017
 */
public class SenhaDto {

    private String senhaAtual;
    private String novaSenha;
    private String confirmacaoSenha;

    public SenhaDto() {

    }

    public String getSenhaAtual() {
        return senhaAtual;
    }

    public void setSenhaAtual(String senhaAtual) {
        this.senhaAtual = senhaAtual;
    }

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }

    public String getConfirmacaoSenha() {
        return confirmacaoSenha;
    }

    public void setConfirmacaoSenha(String confirmacaoSenha) {
        this.confirmacaoSenha = confirmacaoSenha;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.senhaAtual);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SenhaDto other = (SenhaDto) obj;
        if (!Objects.equals(this.senhaAtual, other.senhaAtual)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "";
    }

}
