package br.com.sysodont.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import static br.com.caelum.vraptor.view.Results.json;
import br.com.sysodont.dao.TratamentoDao;
import br.com.sysodont.model.Tratamento;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Classe Controller de {@link Tratamento}.
 *
 * @author allan.richard
 * @since 09/11/2017
 */
@Controller
@Path("/tratamentos")
public class TratamentosController {

    @Inject
    private TratamentoDao dao;

    @Inject
    private Result result;

    @Inject
    private Validator validator;

    @Get("/")
    public List<Tratamento> lista() {
        return dao.listaTodos();
    }

    @Path("/form")
    public void form() {

    }

    @Get("/form/{id}")
    public void form(Long id) {
        Tratamento tratamento = dao.buscaPorId(id);
        if (tratamento != null) {
            result.include("tratamento", tratamento);
        } else {
            result.redirectTo(TratamentosController.class).registroNaoEncontrado();
        }
    }

    @Put("/{tratamento.tratamentoId}")
    public void atualiza(@Valid Tratamento tratamento) {
        dao.atualiza(tratamento);
        result.redirectTo(TratamentosController.class).lista();
    }

    @Post("/")
    public void adiciona(@Valid Tratamento tratamento) {
        dao.adiciona(tratamento);
        result.redirectTo(TratamentosController.class).lista();
    }

    @Get("/form-deleta/{id}")
    public void formDeleta(Long id) {
        Tratamento tratamento = dao.buscaPorId(id);
        result.include("tratamento", tratamento);
    }

    @Delete("/")
    public void deleta(Tratamento tratamento) {
        Tratamento localTratamento = dao.buscaPorId(tratamento.getTratamentoId());
        dao.deleta(localTratamento);
        result.redirectTo(TratamentosController.class).lista();
    }

    @Get("/nao-encontrado")
    public void registroNaoEncontrado() {

    }

    @Get("/por-descricao-json")
    public void buscaPorDescricao(String descricao) {
        List<Tratamento> tratamentos = dao.buscaPorDescricao(descricao);
        if (tratamentos != null) {
            result.use(json()).from(tratamentos)
                    .serialize();
        } else {
            result.use(json()).serializeNulls();
        }
    }

    @Get("/json")
    public void listaJson() {
        List<Tratamento> tratamentos = dao.listaTodos();
        if (tratamentos != null) {
            result.use(json()).from(tratamentos).serialize();
        } else {
            result.use(json()).serializeNulls();
        }
    }
}
