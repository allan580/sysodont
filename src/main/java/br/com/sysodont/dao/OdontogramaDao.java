package br.com.sysodont.dao;

import br.com.sysodont.model.Odontograma;
import br.com.sysodont.model.Pessoa;
import br.com.sysodont.model.QOdontograma;
import br.com.sysodont.model.QPessoa;
import br.com.sysodont.model.QTratamento;
import static br.com.sysodont.model.QTratamento.tratamento;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.math.BigDecimal;
import java.util.List;
import javax.enterprise.context.RequestScoped;

/**
 * Implementação da classe DAO de {@link Pessoa}.
 *
 * @author allan.richard
 * @since 09/11/2017
 */
@RequestScoped
public class OdontogramaDao extends AbstractDao<Odontograma> {

    @Override
    protected Class<Odontograma> getEntityClass() {
        return Odontograma.class;
    }

    /**
     * Busca todos os registros por pessoa e dente.
     *
     * @param pessoa
     * @param idView
     * @return
     */
    public Odontograma buscaPorPessoaTratamentoIdView(Pessoa pessoa, String idView) {
        QOdontograma qOdontograma = QOdontograma.odontograma;
        QTratamento qTratamento = QTratamento.tratamento;
        QPessoa qPessoa = QPessoa.pessoa;

        JPAQueryFactory factory = new JPAQueryFactory(getEntityManager());
        JPAQuery query = factory.query();

        query.select(qOdontograma)
                .from(qOdontograma)
                .join(qOdontograma.pessoa, qPessoa)
                .join(qOdontograma.tratamento, qTratamento)
                .where(qPessoa.eq(pessoa))
                .where(qTratamento.eq(tratamento))
                .where(qOdontograma.idview.eq(idView));

        List<Odontograma> odontogramas = query.fetch();

        return odontogramas.get(0);
    }

    /**
     * Busca todos os registros de uma pessoa.
     *
     * @param pessoa
     * @return odontogramas
     */
    public List<Odontograma> buscaPorPessoa(Pessoa pessoa) {
        QPessoa qPessoa = QPessoa.pessoa;
        QOdontograma qOdontograma = QOdontograma.odontograma;
        QTratamento qTratamento = QTratamento.tratamento;

        JPAQueryFactory factory = new JPAQueryFactory(getEntityManager());
        JPAQuery query = factory.query();

        query.select(qOdontograma)
                .from(qOdontograma)
                .join(qOdontograma.pessoa, qPessoa)
                .join(qOdontograma.tratamento, qTratamento)
                .where(qPessoa.eq(pessoa))
                .orderBy(qTratamento.tratamentoId.asc());

        List<Odontograma> odontogramas = query.fetch();

        return odontogramas;
    }
    
    /**
     * Busca todos os registros de uma pessoa.
     *
     * @param pessoa
     * @param denteId
     * @return odontogramas
     */
    public List<Odontograma> buscaPorPessoaDente(Pessoa pessoa, BigDecimal denteId) {
        QPessoa qPessoa = QPessoa.pessoa;
        QOdontograma qOdontograma = QOdontograma.odontograma;
        QTratamento qTratamento = QTratamento.tratamento;

        JPAQueryFactory factory = new JPAQueryFactory(getEntityManager());
        JPAQuery query = factory.query();

        query.select(qOdontograma)
                .from(qOdontograma)
                .join(qOdontograma.pessoa, qPessoa)
                .join(qOdontograma.tratamento, qTratamento).fetchJoin()
                .where(qPessoa.eq(pessoa))
                .where(qOdontograma.dente.eq(denteId))
                .orderBy(qTratamento.tratamentoId.asc());

        List<Odontograma> odontogramas = query.fetch();

        return odontogramas;
    }
}
