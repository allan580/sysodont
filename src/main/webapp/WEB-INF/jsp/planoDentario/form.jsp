<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Plano Dentário</title> 
    </head>
    <body>
        <c:set var = "formAction" value = "${linkTo[PlanoDentarioController].adiciona(planoDentario)}"/>
        <c:set var = "formMethod" value = "POST"/>
        <c:if test = "${plano.planoId ne null}">
            <c:set var = "formAction" value = "${linkTo[PlanoDentarioController].atualiza(planoDentario)}"/>
            <c:set var = "formMethod" value = "PUT"/>
        </c:if>
        <div class="container">
            <h2>
                Formulário de Plano Dentário
            </h2>
            <hr>
            <form id="formProduto" action="${formAction}" method="POST">
                <div class="container-fluid">
                    <input type="hidden" value="${formMethod}" id="txtMethod" name="_method"/>
                    <input type="hidden" value="${plano.planoId}" id="txtPlanoId" name="planoDentario.planoId"/>
                    <div class="row">
                        <div class="form-group col-lg-8 col-md-8 col-sm-8">
                            <label for="txtNomePlano">Nome Plano</label>
                            <input type="text" value="${plano.nomePlano}" class="form-control" id="txtNomePlano" name="planoDentario.nomePlano" required="required" autofocus="true">
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2">
                            <label for="txtValorMensal">Valor Mensal</label>
                            <input type="number" value="${plano.valorMensal}" class="form-control" id="txtValorMensal" name="planoDentario.valorMensal" required="required" autofocus="true">
                        </div>
                    </div>
                    <div class="row">
                         <div class="form-group col-lg-2 col-md-2 col-sm-2">
                            <label for="txtValorDesconto">Valor Desconto</label>
                            <input type="number" value="${plano.valorDesconto}" class="form-control" id="txtValorDesconto" name="planoDentario.valorDesconto" min="0">
                        </div>
                        <div class="form-group col-lg-8 col-md-8 col-sm-8">
                            <label for="txtSituacao">Situação</label>
                            <input type="text" value="${plano.situacao}" class="form-control" id="txtSituacao" name="planoDentario.situacao" required="required" autofocus="true">
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="form-group" style="margin-top:20px; margin-right: -600px;">
                            <input type="submit" value="Confirmar" class="btn btn-primary btn-sm" />
                            <a href="${linkTo[PlanoDentarioController].lista()}" class="btn btn-default btn-sm"> 
                                Voltar
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!--<script src="<c:url value='/resources/application/planos/form.js'/>"></script>-->
    </body>
</html>