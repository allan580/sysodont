package br.com.sysodont.dao;

import br.com.sysodont.model.Produto;

/**
 * Implementaçao da classe DAO de {@link Produto}.
 *
 * @author andre.baroni
 * @since 28/09/2017
 */
public class ProdutoDao extends AbstractDao<Produto> {

    @Override
    protected Class<Produto> getEntityClass() {
        return Produto.class;
    }
}
