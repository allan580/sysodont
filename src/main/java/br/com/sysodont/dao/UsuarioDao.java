package br.com.sysodont.dao;

import br.com.sysodont.model.QUsuario;
import br.com.sysodont.model.Usuario;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import javax.enterprise.context.RequestScoped;

/**
 * Implementação da classe DAO de {@link Usuario}.
 *
 * @author allan.richard
 * @since 02/11/2017
 */
@RequestScoped
public class UsuarioDao extends AbstractDao<Usuario>{

    @Override
    protected Class<Usuario> getEntityClass() {
        return Usuario.class;
    }

    /**
     * Valida se o usuário e a senha fornecidos são válidos.
     *
     * @param usuario
     * @return
     */
    public Usuario validaLoginSenha(Usuario usuario) {
        QUsuario qUsuario = QUsuario.usuario;

        JPAQueryFactory factory = new JPAQueryFactory(this.getEntityManager());
        JPAQuery query = factory.query();
        query.select(qUsuario)
                .from(qUsuario)
                .where(qUsuario.login.eq(usuario.getLogin()))
                .where(qUsuario.senha.eq(usuario.getSenha()));

        List<Usuario> usuarios = query.fetch();

        if (usuarios.size() > 0) {
            return usuarios.get(0);
        } else {
            return null;
        }
    }

    public Usuario buscaPorLogin(String login) {
        QUsuario qUsuario = QUsuario.usuario;

        JPAQueryFactory factory = new JPAQueryFactory(this.getEntityManager());
        JPAQuery query = factory.query();
        query.select(qUsuario)
                .from(qUsuario)
                .where(qUsuario.login.eq(login));

        List<Usuario> usuarios = query.fetch();

        if (usuarios.size() > 0) {
            return usuarios.get(0);
        } else {
            return null;
        }
    }
}
