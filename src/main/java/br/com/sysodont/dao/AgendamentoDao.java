package br.com.sysodont.dao;

import br.com.sysodont.model.Agendamento;
import br.com.sysodont.model.QAgendamento;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import javax.enterprise.context.RequestScoped;

/**
 * Implementação da classe DAO de {@link Agendamento}.
 *
 * @author andre.baroni
 * @since 02/10/2017
 */
@RequestScoped
public class AgendamentoDao extends AbstractDao<Agendamento> {

    /**
     * Ao receber pelo menos uma letra, retorna todos os cargos que se contenha
     * tal letra, ou letras.
     *
     * @param nome
     * @return agendamentos
     */
    /*public List<Agendamento> buscaPorData(String descricao) {
        if (descricao.length() == 0) {
            return null;
        }

        QAgendamento qAgendamento = QAgendamento.agendamento;

        JPAQueryFactory factory = new JPAQueryFactory(this.getEntityManager());
        JPAQuery query = factory.query();
        query.select(qAgendamento)
                .from(qAgendamento)
                .where(qAgendamento.dataAgendada.contains(descricao));

        List<Agendamento> grupos = query.fetch();

        if (grupos.size() > 0) {
            return grupos;
        } else {
            return null;
        }
    }*/
    public List<Agendamento> buscaPorData(Agendamento agendamento) {
        QAgendamento qAgendamento = QAgendamento.agendamento;

        JPAQueryFactory factory = new JPAQueryFactory(getEntityManager());
        JPAQuery query = factory.query();

        query.select(qAgendamento)
                .from(qAgendamento)
                .where(qAgendamento.dataHoraAgendada.eq(agendamento.getDataHoraAgendada()));

        List<Agendamento> agendamentos = query.fetch();

        return agendamentos;
    }
    
    @Override
    protected Class<Agendamento> getEntityClass() {
        return Agendamento.class;
    }
}
