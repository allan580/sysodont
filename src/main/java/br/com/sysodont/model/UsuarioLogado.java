package br.com.sysodont.model;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author allan.richard
 * @since 02/11/2017
 */
@SessionScoped
@Named
public class UsuarioLogado implements Serializable {

    private Usuario usuario;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public boolean estaLogado() {
        return usuario != null ? true : false;
    }
}
