<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Agendamento</title>
    </head>
    <body>
        <div class="row">
            <div class="col-md-8">
                <h2>Agendamentos</h2>
            </div>
            <div class="col-md-4 text-right">
                <a href="${linkTo[AgendamentoController].form()}">
                    <button class="btn btn-primary" style="margin-top:20px;"><i class="fa fa-plus" aria-hidden="true"></i>Novo Agendamento</button>
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table id="listaAgendamento" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th >ID</th>
                        <th>Pessoa</th>
                        <th>Data/hora marcada</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${agendamentoList}" var="agendamento">
                        <tr>
                            <td> ${agendamento.agendamentoId} </td>
                            <td> ${agendamento.pessoas.nome} </td>
                            <td> <fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${agendamento.dataHoraAgendada}" /></td>
                            <td> 
                                <div class="btn-group">
                                    <a href="${linkTo[AgendamentoController].form(agendamento.agendamentoId)}" id="btnAgendamentoAtualiza_${agendamento.agendamentoId}" class="btn btn-default">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                    <a href="${linkTo[AgendamentoController].formDeleta(agendamento.agendamentoId)}" id="btnAgendamentoDeleta_${agendamento.agendamentoId}" type="button" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <script src="<c:url value='/resources/application/agendamento/lista.js'/>"></script>
    </body>
</html>
