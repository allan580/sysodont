<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Plano Dentário</title>     
    </head>
    <body>
        <div class="container">
            <div>
                <h2>
                    Remover Plano
                </h2>
            </div>
            <hr>
            <p>
                Realmente deseja remover o plano '${planoDentario.nomePlano}'?
            </p>
            <form id="formProduto" action="${linkTo[PlanoDentarioController].deleta()}" method="POST">
                <div class="row">
                    <input class="form-control" type="hidden" id="txtMethod" name="_method" value="DELETE"/><br/>
                    <input type="hidden" value="${planoDentario.planoId}" id="txtPlanoId" name="planoDentario.planoId"/><br/> 
                </div>
                <div class="btn-group">
                    <input type="submit" value="Deletar" class="btn btn-danger" />
                    <a href="${linkTo[PlanoDentarioController].lista()}" class="btn btn-default" >Voltar</a>
                </div>
            </form>
        </div>
    </body>
</html>
