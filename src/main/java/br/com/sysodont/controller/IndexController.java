package br.com.sysodont.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;

/**
 * Classe controller do index.
 *
 * @author andre.baroni
 * @since 27/09/2017
 */
@Controller
public class IndexController {

    private final Result result;

    /**
     * @deprecated CDI eyes only
     */
    protected IndexController() {
        this(null);
    }

    @Inject
    public IndexController(Result result) {
        this.result = result;
    }

    @Path("/")
    public void index() {
        
    }
}
