<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Pagamentos</title>
    </head>
    <body>
        <div class="row">
            <div class="col-md-8">
                <h2>Pagamentos</h2>
            </div>
            <div class="col-md-4 text-right">
                <a href="${linkTo[PagamentoController].form()}">
                    <button class="btn btn-primary" style="margin-top:20px;"><i class="fa fa-plus" aria-hidden="true"></i>Novo Pagamento</button>
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table id="listaPagamento" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Pessoa</th>
                        <th>Valor Total</th>
                        <th>Quantidade Parcela</th>
                        <th>Valor Parcela</th>
                        <th>Data Vencimento</th>
                        <th>Operacões</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${pagamentoList}" var="pagamentos">
                        <tr>
                            <td> ${pagamentos.pagamentoId} </td>
                            <td> ${pagamentos.odontograma.pessoa.nome} </td>
                            <td> ${pagamentos.valorTotal} </td>
                            <td> ${pagamentos.quantidadeParcela} </td>
                            <td> ${pagamentos.valorParcela} </td>
                            <td> <fmt:formatDate dateStyle = "short" value = "${pagamentos.dataVencimento}" /></td>
                            <td> 
                                <div class="btn-group">
                                    <a href="${linkTo[PagamentoController].formDeleta(pagamentos.pagamentoId)}" id="btnPagamentoDeleta_${pagamentos.pagamentoId}" type="button" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <script src="<c:url value='/resources/application/pagamento/lista.js'/>"></script>
    </body>
</html>