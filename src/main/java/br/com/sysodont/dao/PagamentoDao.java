package br.com.sysodont.dao;

import br.com.sysodont.model.Pagamento;
import br.com.sysodont.model.Pessoa;
import br.com.sysodont.model.QOdontograma;
import br.com.sysodont.model.QPagamento;
import br.com.sysodont.model.QPessoa;
import br.com.sysodont.model.QTratamento;
import br.com.sysodont.model.Tratamento;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import javax.enterprise.context.RequestScoped;

/**
 * Implementação da classe DAO de {@link Pessoa}.
 *
 * @author allan.richard
 * @since 02/10/2017
 */
@RequestScoped
public class PagamentoDao extends AbstractDao<Pagamento> {

    /**
     * Ao receber pelo menos uma letra, retorna todos os cargos que se contenha
     * tal letra, ou letras.
     *
     * @param nome
     * @return pessoas
     */
    @Override
    protected Class<Pagamento> getEntityClass() {
        return Pagamento.class;
    }

    public List<Pagamento> buscaPorPessoa(Pessoa pessoa) {
        QPagamento qPagamento = QPagamento.pagamento;
        QOdontograma qOdontograma = QOdontograma.odontograma;
        QPessoa qPessoa = QPessoa.pessoa;
        QTratamento qTratamento = QTratamento.tratamento;

        JPAQueryFactory factory = new JPAQueryFactory(getEntityManager());
        JPAQuery query = factory.query();

        query.select(qPagamento)
                .from(qPagamento)
                .join(qPagamento, qOdontograma).fetchJoin()
                .join(qOdontograma.pessoa, qPessoa).fetchJoin()
                .join(qOdontograma.tratamento, qTratamento).fetchJoin()
                .where(qPessoa.eq(pessoa))
                .orderBy(qPagamento.pagamentoId.asc());

        List<Pagamento> pagamentos = query.fetch();

        return pagamentos;
    }

    public List<Tratamento> buscaTratamentoPessoa(Pessoa pessoa) {
        QOdontograma qOdontograma = QOdontograma.odontograma;
        QPessoa qPessoa = QPessoa.pessoa;
        QTratamento qTratamento = QTratamento.tratamento;

        JPAQueryFactory factory = new JPAQueryFactory(getEntityManager());
        JPAQuery query = factory.query();

        query.select(qTratamento)
                .from(qTratamento)
                .leftJoin(qTratamento.odontograma, qOdontograma).fetchJoin()
                .leftJoin(qOdontograma.pessoa, qPessoa).fetchJoin()
                .where(qPessoa.eq(pessoa));

        List<Tratamento> tratamentos = query.fetch();

        return tratamentos;
    }

}
