<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Plano</title>
    </head>
    <body>
        <div class="row">
            <div class="col-md-8">
                <h2>Planos</h2>
            </div>
            <div class="col-md-4 text-right">
                <a href="${linkTo[PlanoDentarioController].form()}">
                    <button class="btn btn-primary" style="margin-top:20px;"><i class="fa fa-plus" aria-hidden="true"></i>Novo Plano</button>
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table id="listaPlano" class="table table-striped table-bordered " cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="col-2">ID</th>
                        <th>Nome Plano</th>
                        <th>Valor Mensal</th>                
                        <th>Valor Desconto</th>
                        <th>Situação</th>
                        <th>Operações</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${planoDentarioList}" var="plano">
                        <tr>
                            <td> ${plano.planoId} </td>
                            <td> ${plano.nomePlano} </td>
                            <td> ${plano.valorMensal} </td>
                            <td> ${plano.valorDesconto} </td>
                            <td> ${plano.situacao} </td>
                            <td> 
                                <div class="btn-group">
                                    <a href="${linkTo[PlanoDentarioController].form(plano.planoId)}" id="btnPlanoAtualiza_${plano.planoId}" class="btn btn-default">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                    <a href="${linkTo[PlanoDentarioController].formDeleta(plano.planoId)}" id="btnPlanoDeleta_${plano.planoId}" type="button" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <script src="<c:url value='/resources/application/planos/lista.js'/>"></script>
    </body>
</html>