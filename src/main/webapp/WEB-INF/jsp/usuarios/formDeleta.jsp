<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Usuários</title>     
    </head>
    <body>
        <div class="container">
            <div>
                <h2>
                    Remover Usuário
                </h2>
            </div>
            <hr>
            <p>
                Realmente deseja remover o usuário '${usuario.login}'?
            </p>
            <form id="formProduto" action="${linkTo[UsuariosController].deleta()}" method="POST">
                <div class="row">
                    <input class="form-control" type="hidden" id="txtMethod" name="_method" value="DELETE"/><br/>
                    <input type="hidden" value="${usuario.usuarioId}" id="txtUsuarioId" name="usuario.usuarioId"/><br/> 
                </div>
                    <div class="btn-group">
                        <input type="submit" value="Deletar" class="btn btn-danger" />
                        <a href="${linkTo[UsuariosController].lista()}" class="btn btn-primary" >Voltar</a>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>