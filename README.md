#TMST

	Esse projeto foi iniciado como trabalho de conclusão de curso (TCC) da faculdade Faculdades Integradas de Bauru (FIB), grupo 6 da turma de Ciencias da Computação 2014.

	Por falta de tempo, não foi possível criar o projeto para a API e o projeto para a interface do usuário, logo num mesmo projeto, é distribuido o front-end e o back-end.

##O que é necessário para iniciar?
	Java Jdk 8 ou superior.

	Apache Tomcat 8 ou superior.

	PostgreSql, com um banco de dados chamado sysodont.

##Tecnologias
	+ Back-end.java: CDI, Bean-Validation.
	+ Back-end.mvc: VRaptor 4.
	+ Back-end.persistencia: Hibernate JPA, VRaptor JPA, QueryDSL.
	+ Back-end.tests: não implementado.
	+ Front-end.template: JSTL, sitemesh3.
	+ Front-end.style: Bootstrap.
	+ Front-end.tests: não implementado.
	
##Creditos
	+ André Richard de Assis Baroni.
	+ Allan Richard Fracaroli da Silva.
	+ Luiz Henrique Derenzi.