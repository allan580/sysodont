package br.com.sysodont.dao;

import br.com.sysodont.model.Pessoa;
import br.com.sysodont.model.PlanoDentario;
import br.com.sysodont.model.QPessoa;
import br.com.sysodont.model.QPlanoDentario;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import javax.enterprise.context.RequestScoped;

/**
 * Implementação da classe DAO de {@link Pessoa}.
 *
 * @author andre.baroni
 * @since 02/10/2017
 */
@RequestScoped
public class PessoaDao extends AbstractDao<Pessoa> {

    /**
     * Ao receber pelo menos uma letra, retorna todos os cargos que se contenha
     * tal letra, ou letras.
     *
     * @param nome
     * @return pessoas
     */
    public List<Pessoa> buscaPorNome(String nome) {
        if (nome.length() == 0) {
            return null;
        }

        QPessoa qPessoa = QPessoa.pessoa;

        JPAQueryFactory factory = new JPAQueryFactory(this.getEntityManager());
        JPAQuery query = factory.query();
        query.select(qPessoa)
                .from(qPessoa)
                .where(qPessoa.nome.contains(nome));

        List<Pessoa> pessoas = query.fetch();

        if (pessoas.size() > 0) {
            return pessoas;
        } else {
            return null;
        }
    }

    @Override
    protected Class<Pessoa> getEntityClass() {
        return Pessoa.class;
    }

    public PlanoDentario getPlano(Pessoa pessoa) {
        QPlanoDentario qPlanoDentario = QPlanoDentario.planoDentario;
        QPessoa qPessoa = QPessoa.pessoa;

        JPAQueryFactory factory = new JPAQueryFactory(getEntityManager());
        JPAQuery query = factory.query();
        query.select(qPlanoDentario)
                .from(qPlanoDentario)
                .join(qPlanoDentario.pessoa, qPessoa)
                .where(qPessoa.pessoaId.eq(pessoa.getPessoaId()));

        List<PlanoDentario> planos = query.fetch();

        if (planos.size() > 0) {
            return planos.get(0);
        } else {
            return null;
        }
    }
}
