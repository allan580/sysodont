<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="decorator" content="blank" />
        <title>
            Login
        </title>
    </head>
    <body hold-transition login-page>
        <div class="login-box">
            <div class="login-logo">
                <a href="#">SysOdont - Sistema Odontológico</a>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">Bem-vindo ao sistema!</p>
                <c:if test="${errors ne null}">
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <c:forEach items="${errors}" var="erro">
                            ${erro.category} - ${erro.message} <br/>
                        </c:forEach>
                    </div>
                </c:if>
                <form action="${linkTo[LoginController].autentica()}" method="POST">
                    <div class="form-group has-feedback">
                        <label for="txtUsuarioLogin">Usuário</label>
                        <input type="text" class="form-control" placeholder="Login" name="usuario.login" required>
                        <span class="form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="txtPassword">Senha</label>
                        <input type="password" class="form-control" placeholder="Senha" name="usuario.senha" required>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <input type="submit" class="btn btn-primary btn-block" value="Login">
                </form>
            </div>
        </div>
    </div>
</body>
</html>