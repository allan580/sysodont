package br.com.sysodont.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe modelo de Tratamento.
 *
 * @author allan.richard
 * @since 04/10/2017
 */
@Entity
@Table(name = "tratamento")
public class Tratamento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tratamento_id")
    private Long tratamentoId;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "preco")
    private Double preco;
    
    @OneToMany(mappedBy = "tratamento", fetch = FetchType.LAZY)
    private List<Odontograma> odontograma;
    
    
    public Tratamento() {

    }

    public Tratamento(Long tratamentoId) {
        this.tratamentoId = tratamentoId;
    }

    public Long getTratamentoId() {
        return tratamentoId;
    }

    public void setTratamentoId(Long tratamentoId) {
        this.tratamentoId = tratamentoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public List<Odontograma> getOdontograma() {
        return odontograma;
    }

    public void setOdontograma(List<Odontograma> odontograma) {
        this.odontograma = odontograma;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.tratamentoId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tratamento other = (Tratamento) obj;
        if (!Objects.equals(this.tratamentoId, other.tratamentoId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descricao == null ? "" : descricao;
    }
    
}
