package br.com.sysodont.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.com.sysodont.dao.AgendamentoDao;
import br.com.sysodont.model.Agendamento;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Classe Controller de {@link Agendamento}.
 *
 * @author allan.richard/luiz derenzi
 * @since 05/10/2017
 */
@Controller
@Path("/agendamentos")
public class AgendamentoController {

    @Inject
    private AgendamentoDao dao;

    @Inject
    private Result result;

    @Inject
    private Validator validator;

    @Get("/")
    public List<Agendamento> lista() {
        return dao.listaTodos();
    }

    @Get("/form")
    public void form() {

    }

    @Get("/form/{id}")
    public void form(Long id) {
        Agendamento agendamento = dao.buscaPorId(id);
        if (agendamento != null) {
            result.include("agendamento", agendamento);
        } else {
            result.redirectTo(AgendamentoController.class).registroNaoEncontrado();
        }
    }

    @Post("/")
    public void adiciona(@Valid Agendamento agendamento) {
        List<Agendamento> agendamentos = dao.buscaPorData(agendamento);
            if (agendamentos.size() > 0) {
                validator.add(new SimpleMessage("Autenticação", "Horário já agendado, selecione um novo!"));
                validator.onErrorRedirectTo(this).form();
            }
        dao.adiciona(agendamento);
        result.redirectTo(AgendamentoController.class).lista();
    }

    @Put("/{pessoa.pessoaId}")
    public void atualiza(@Valid Agendamento agendamento) {
        dao.atualiza(agendamento);
        result.redirectTo(AgendamentoController.class).lista();
    }

    @Get("/form-deleta/{id}")
    public void formDeleta(Long id) {
        Agendamento agendamento = dao.buscaPorId(id);
        result.include("agendamento", agendamento);
    }

    @Delete("/")
    public void deleta(Agendamento agendamento) {
        Agendamento localAgendamento = dao.buscaPorId(agendamento.getAgendamentoId());
        dao.deleta(localAgendamento);
        result.redirectTo(AgendamentoController.class).lista();
    }

    @Get("/nao-encontrado")
    public void registroNaoEncontrado() {

    }
}
