package br.com.sysodont.dao;

import br.com.sysodont.model.Tratamento;
import br.com.sysodont.model.QTratamento;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import javax.enterprise.context.RequestScoped;

/**
 * Implementação da classe DAO de {@link Tratamento}.
 *
 * @author andre.baroni
 * @since 28/09/2017
 */
@RequestScoped
public class TratamentoDao extends AbstractDao<Tratamento> {

    /**
     * Ao receber pelo menos uma letra, retorna todos os grupos que se contenha
     * tal letra, ou letras.
     *
     * @param descricao
     * @return tratamentos
     */
    public List<Tratamento> buscaPorDescricao(String descricao) {
        if (descricao.length() == 0) {
            return null;
        }

        QTratamento qTratamento = QTratamento.tratamento;

        JPAQueryFactory factory = new JPAQueryFactory(this.getEntityManager());
        JPAQuery query = factory.query();
        query.select(qTratamento)
                .from(qTratamento)
                .where(qTratamento.descricao.contains(descricao));

        List<Tratamento> grupos = query.fetch();

        if (grupos.size() > 0) {
            return grupos;
        } else {
            return null;
        }
    }

    @Override
    protected Class<Tratamento> getEntityClass() {
        return Tratamento.class;
    }
}
