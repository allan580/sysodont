$(document).ready(function () {
    $('#selPessoa').select2({
        ajax: {
            url: $('#pathPessoaPorNomeJson').val(),
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    nome: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
                return {
                    results: $.map(data.list, function (item) {
                        return {
                            'nome': item.nome,
                            'pessoaId': item.pessoaId,
                            text: item.nome,
                            id: item.pessoaId
                        }
                    })
                };
            },
            cache: true
        },
        allowClear: true,
        placeholder: 'Busca de pessoas',
        minimumInputLength: 2
    });
});