<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
    <head>
        <title> - Pagamentos</title>
    </head>
    <body>
        <c:set var = "formAction" value = "${linkTo[PagamentoController].adiciona()}"/>
        <c:set var = "formMethod" value = "POST"/>
        <c:if test = "${pagamentos.pagamentoId ne null}">
            <c:set var = "formAction" value = "${linkTo[PagamentoController].atualiza(pessoa)}"/>
            <c:set var = "formMethod" value = "PUT"/>
        </c:if>
        <div class="container">
            <h2>
                Formulário de Pagamento
            </h2>
            <hr>
            <div class="container-fluid">
                <form id="formPagamento" action="${formAction}" method="POST">
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="selPessoa">Pessoa</label>
                            <select id="selPessoa" class="form-control" name="pessoaId" required>
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2">
                            <label for="selNumParcela">Qtd Parcelas</label>
                            <select id="selNumParcela" class="form-control" name="pagamento.quantidadeParcela" required>
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <label for="numValorTotalPagamento">Valor Total</label>
                            <input type="number" value="${pagamentos.valorTotal}" class="form-control" id="numValorTotalPagamento" name="pagamento.valorTotal" required="required">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class='input-group date' id='datetimepicker'>
                                <label for="diaVencimento">Dia Vencimento</label>
                                <input type='number' class="form-control" value="" name="diaVencimento" min="1" max="28" required/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-left">
                        <div class="form-group" style="margin-top:20px; margin-right: -200px;">
                            <input type="submit" value="Confirmar" class="btn btn-primary btn-sm"/>
                            <a href="${linkTo[PagamentoController].lista()}" class="btn btn-default btn-sm"> 
                                Voltar
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">

            function atribuiValorTotalInput() {
                $.ajax({
                    url: $('#pathPessoaValorPagarJson').val() + '/' + $('#selPessoa').val(),
                    type: 'GET',
                    success: function (data) {
                        $('#numValorTotalPagamento').val(data['bigDecimal']);
                    }
                });
            }

            //$('#selNumParcela').select2();




            $('#selNumParcela').on('select2:select', function (e) {
                if ($('#selPessoa').val() > 0) {
                    alert('selecione uma pessoa');
                    return false;
                }
                inserePagamento();
            });

            $('#selPessoa').on('select2:select', function (e) {
                if ($('#selPessoa').val() > 0) {
                    atribuiValorTotalInput();
                }
            });

            function inserePagamento(idDenteLado) {
                $.ajax({
                    url: $('#pathOdontogramaAdiciona').val(),
                    type: 'POST',
                    data: {
                        'odontograma.paciente.pessoaId': $('#selPessoa').val(),
                        'odontograma.tratamento.tratamentoId': $('#selTratamento').val(),
                        'odontograma.idview': idDenteLado
                    },
                    success: function () {

                    }
                });
            }
        </script>                       
        <input type="hidden" id="pathPessoaPorNomeJson" value="${linkTo[PessoasController].buscaPorNome()}" />
        <input type="hidden" id="pathPessoaValorPagarJson" value="${linkTo[PagamentoController].listaJson()}" />
        <input type="hidden" id="pathOdontogramaAdiciona" value="${linkTo[OdontogramaController].adiciona()}" />
        <script src="<c:url value='/resources/application/pagamento/form.js'/>"></script>
    </body>
</html>