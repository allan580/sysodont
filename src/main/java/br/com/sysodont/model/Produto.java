package br.com.sysodont.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Classe modelo de Produto.
 *
 * @author andre.baroni
 * @since 27/09/2017
 */
@Entity
@Table(name = "produto")
public class Produto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "produto_id")
    private Long produtoId;

    @Column(name = "descricao")
    @NotNull
    private String descricao;

    @Column(name = "quantidade_produto")
    private BigDecimal quantidadeProduto;

    @Column(name = "preco")
    private BigDecimal preco;

    /*@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grupo_produto_id", referencedColumnName = "grupo_produto_id")
    private GrupoProduto grupoProduto;*/
    public Produto() {

    }

    public Produto(Long produtoId) {
        this.produtoId = produtoId;
    }

    public Long getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(Long produtoId) {
        this.produtoId = produtoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public BigDecimal getQuantidadeProduto() {
        return quantidadeProduto;
    }

    public void setQuantidadeProduto(BigDecimal quantidadeProduto) {
        this.quantidadeProduto = quantidadeProduto;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.produtoId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produto other = (Produto) obj;
        if (!Objects.equals(this.produtoId, other.produtoId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.descricao == null ? "" : this.descricao;
    }

}
