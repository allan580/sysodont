<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Tratamento</title> 
    </head>
    <body>
        <div class="container">
            <h1>
                404 - Não Encontrado
            </h1>
            <h4>
                Ops... não conseguimos encontrar o Tratamento!
            </h4>
            <a href="${linkTo[TratamentosController].lista()}" class="btn btn-default" >voltar</a>
        </div>
    </body>
</html>
