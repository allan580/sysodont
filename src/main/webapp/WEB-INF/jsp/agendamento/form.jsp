<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Agendamento</title> 
    </head>
    <body>
        <c:set var = "formAction" value = "${linkTo[AgendamentoController].adiciona()}"/>
        <c:set var = "formMethod" value = "POST"/>
        <c:if test = "${agendamento.agendamentoId ne null}">
            <c:set var = "formAction" value = "${linkTo[AgendamentoController].atualiza(agendamento)}"/>
            <c:set var = "formMethod" value = "PUT"/>
        </c:if>
        <div class="container">
            <h2>
                Formulário de Agendamento
            </h2>
            <hr>
            <div class="container">
                <div class="row">
                    <c:if test="${errors ne null}">
                        <div class="alert alert-warning" style="width: 200px">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <c:forEach items="${errors}" var="erro">
                                ${erro.category} - ${erro.message} <br/>
                            </c:forEach>
                        </div>
                    </c:if>
                </div>
            </div>
            <form id="formAgendamento" action="${formAction}" method="POST">
                <div class="container-fluid">
                    <input type="hidden" value="${formMethod}" id="txtMethod" name="_method"/>
                    <input type="hidden" value="${agendamento.agendamentoId}" id="txtAgendamentoId" name="agendamento.agendamentoId"/>
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="selPessoa">Pessoa</label>
                            <select id="selPessoa" class="form-control" name="agendamento.pessoas.pessoaId" required>
                                <option value=""></option>
                                <c:if test="${agendamento.pessoas ne null}" >
                                    <option value="${agendamento.pessoas.pessoaId}" selected="selected">${agendamento.pessoas.nome}</option>
                                </c:if>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <div class='input-group date' id='datetimepicker'>
                                <input type='text' class="form-control" value="${agendamento.dataHoraAgendada}" name="agendamento.dataHoraAgendada" required/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="form-group" style="margin-top:30px; margin-right: 210px;">
                            <input type="submit" value="Confirmar" class="btn btn-primary btn-sm"/>
                            <a href="${linkTo[AgendamentoController].lista()}" class="btn btn-default btn-sm"> 
                                Voltar
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker({
                    useCurrent: true,
                    format: 'YYYY-MM-DD HH',
                    locale: 'pt-br',
                    minDate: new Date(),
                    sideBySide: true
                }).on('dp.show', function () {
                    $('a.btn[data-action="incrementMinutes"], a.btn[data-action="decrementMinutes"]').removeAttr('data-action').attr('disabled', true);
                    $('span.timepicker-minute[data-action="showMinutes"]').removeAttr('data-action').attr('disabled', true).text('00');
                }).on('dp.change', function () {
                    $(this).val($(this).val().split(':')[0] + ':00');
                    $('span.timepicker-minute').text('00');
                });
            });
        </script>
        <input type="hidden" id="pathPessoaPorNomeJson" value="${linkTo[PessoasController].buscaPorNome()}" />
        <script src="<c:url value='/resources/application/agendamento/form.js'/>"></script>
    </body>
</html>
