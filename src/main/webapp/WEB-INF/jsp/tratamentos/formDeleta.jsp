<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Tratamento</title>     
    </head>
    <body>
        <div class="container">
            <div>
                <h2>
                    Remover Tratamento
                </h2>
            </div>
            <hr>
            <p>
                Deseja remover o tratamento '${tratamento.descricao}'?
            </p>
            <form id="formTratamento" action="${linkTo[TratamentosController].deleta()}" method="POST">
                <div class="row">
                    <input class="form-control" type="hidden" id="txtMethod" name="_method" value="DELETE"/><br/>
                    <input type="hidden" value="${tratamento.tratamentoId}" id="txtTratamentoId" name="tratamento.tratamentoId"/><br/> 
                </div>
                <div class="btn-group">
                    <input type="submit" value="Deletar" class="btn btn-danger" />
                    <a href="${linkTo[TratamentosController].lista()}" class="btn btn-default" >Voltar</a>
                </div>
            </form>
        </div>
    </body>
</html>
