package br.com.sysodont.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import static br.com.caelum.vraptor.view.Results.json;
import br.com.sysodont.dao.OdontogramaDao;
import br.com.sysodont.dao.PessoaDao;
import br.com.sysodont.model.Odontograma;
import br.com.sysodont.model.Pessoa;
import java.math.BigDecimal;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Classe Controller de {@link Pessoa}.
 *
 * @author allan.richard/luiz derenzi
 * @since 09/11/2017
 */
@Controller
@Path("/odontograma")
public class OdontogramaController {

    @Inject
    private OdontogramaDao dao;
    
    @Inject
    private PessoaDao pessoaDao;

    @Inject
    private Result result;

    @Inject
    private Validator validator;

    @Get("/")
    public List<Odontograma> lista() {
        return dao.listaTodos();
    }

    @Get("/json/filtro/pessoa")
    public void listaJson(Long pessoaId) {
        Pessoa pessoa = pessoaDao.buscaPorId(pessoaId);
        List<Odontograma> odonts = dao.buscaPorPessoa(pessoa);
        if (odonts != null) {
            result.use(json()).from(odonts).serialize();
        } else {
            result.use(json()).serializeNulls();
        }
    }
    
    @Get("/json/filtro/pessoa/{pessoaId}/dente/{denteId}")
    public void listaJsonPessoaDente(Long pessoaId, Long denteId) {
        Pessoa pessoa = pessoaDao.buscaPorId(pessoaId);
        List<Odontograma> odonts = dao.buscaPorPessoaDente(pessoa, new BigDecimal(String.valueOf(denteId)));
        if (odonts != null) {
            result.use(json())
                    .from(odonts)
                    .include("tratamento")
                    .serialize();
        } else {
            result.use(json())
                    .serializeNulls();
        }
    }

    @Path("/form")
    public void form() {

    }

    @Get("/form/{id}")
    public void form(Long id) {
        Odontograma odontograma = dao.buscaPorId(id);
        if (odontograma != null) {
            result.include("odontograma", odontograma);
        } else {
            result.redirectTo(OdontogramaController.class).registroNaoEncontrado();
        }
    }

    @Put("/{pessoa.pessoaId}")
    public void atualiza(@Valid Odontograma odontograma) {
        dao.atualiza(odontograma);
        result.redirectTo(OdontogramaController.class).lista();
    }

    @Post("/")
    public void adiciona(@Valid Odontograma odontograma) {
        String dente = odontograma.getIdview().substring(1, odontograma.getIdview().indexOf('f'));
        String lado = odontograma.getIdview().substring(odontograma.getIdview().indexOf('f') + 1, odontograma.getIdview().length());
        
        odontograma.setDente(new BigDecimal(dente));
        odontograma.setLadoDente(lado);
        dao.adiciona(odontograma);
        result.redirectTo(OdontogramaController.class).lista();
    }

    @Get("/form-deleta/{id}")
    public void formDeleta(Long id) {
        Odontograma odontograma = dao.buscaPorId(id);
        result.include("odontograma", odontograma);
    }

    @Delete("/")
    public void deleta(Odontograma odontograma) {
        Odontograma localOdontograma = dao.buscaPorPessoaTratamentoIdView(odontograma.getPessoa(), odontograma.getIdview());
        dao.deleta(localOdontograma);
        result.redirectTo(OdontogramaController.class).lista();
    }

    @Get("/nao-encontrado")
    public void registroNaoEncontrado() {

    }
}
