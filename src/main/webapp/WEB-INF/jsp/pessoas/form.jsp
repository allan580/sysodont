<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> - Pessoas</title> 
    </head>
    <body>
        <c:set var = "formAction" value = "${linkTo[PessoasController].adiciona()}"/>
        <c:set var = "formMethod" value = "POST"/>
        <c:if test = "${pessoa.pessoaId ne null}">
            <c:set var = "formAction" value = "${linkTo[PessoasController].atualiza(pessoa)}"/>
            <c:set var = "formMethod" value = "PUT"/>
        </c:if>
        <div class="container">
            <h2>
                Formulário de Pessoas
            </h2>
            <hr>
            <form id="formPessoa" action="${formAction}" method="POST">
                <div class="container-fluid">
                    <div class="row">
                        <input type="hidden" value="${formMethod}" id="txtMethod" name="_method"/>
                        <input type="hidden" value="${pessoa.pessoaId}" id="txtPessoaId" name="pessoa.pessoaId"/>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="txtPessoaNome">Nome</label>
                            <input type="text" value="${pessoa.nome}" class="form-control" id="txtPessoaNome" name="pessoa.nome" required="required"  autofocus="true">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="txtPessoaCPF">CPF</label>
                            <input type="text" value="${pessoa.cpf}" class="form-control" id="txtPessoaCPF" name="pessoa.cpf" required="required">
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="txtPessoaRG">RG</label>
                            <input type="text" value="${pessoa.rg}" class="form-control" id="txtPessoaRG" name="pessoa.rg">
                        </div> 
                        <div class="form-group col-lg-2 col-md-2 col-sm-2">
                            <label for="txtPessoaDataNascimento">Data Nascimento</label>
                            <input type="date" value="${pessoa.dataNascimento}" class="form-control" id="txtPessoaDataNascimento" name="pessoa.dataNascimento">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="txtPessoaTelefone">Telefone</label>
                            <input type="text" value="${pessoa.telefone}" class="form-control" id="txtPessoaTelefone" name="pessoa.telefone">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <label for="selPlano">Plano</label>
                            <select id="selPlano" class="form-control" name="pessoa.plano.planoId">
                                <option value=""></option>
                                <c:if test="${pessoa.plano ne null}" >
                                    <option value="${pessoa.plano.planoId}" selected="selected">${pessoa.plano.nomePlano}</option>
                                </c:if>
                            </select>
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="form-group" style="margin-top:20px; margin-right: -200px;">
                                <input type="submit" value="Confirmar" class="btn btn-primary btn-sm"/>
                                <a href="${linkTo[PessoasController].lista()}" class="btn btn-default btn-sm"> 
                                    Voltar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
</div>
<input type="hidden" id="pathPlanoPorDescricaoJson" value="${linkTo[PlanoDentarioController].buscaPorNome()}" />
<script src="<c:url value='/resources/application/pessoas/form.js'/>"></script>
</body>
</html>
